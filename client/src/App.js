import React from 'react';
import './App.css';


import ApolloClient from 'apollo-boost';

import { ApolloProvider } from '@apollo/react-hooks';

import Request from './request/request.js';


const client = new ApolloClient({
  uri: 'http://localhost:4000/graphql',
});


function App() {
  return (
    <ApolloProvider client={client}>

        <Request />

    </ApolloProvider>
  );
}

export default App;
