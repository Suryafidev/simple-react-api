import React, { Component, Fragment} from 'react';
import { gql } from "apollo-boost";
import { Query } from 'react-apollo'
import { ListGroup, Tab, Nav, Row, Col } from 'react-bootstrap';
import Oreoitem from './Oreoitem';



const FitureTalk = gql`
    {
      FitureTalk {
        id
        name
        description
      }
    }
  `;


const Oreo = gql`
    {
      Oreo {
        id
        name
        description
      }
    }
  `;

export default class Request extends Component {
  render() {
    return (
  <Tab.Container id="left-tabs-example" defaultActiveKey="oreo">
    <Row>
      <Col sm={2}>
        <Nav variant="pills" className="flex-column">
          <Nav.Item>
            <Nav.Link eventKey="oreo">Makan Oreo</Nav.Link>
          </Nav.Item>
          
        </Nav>
      </Col>
      <Col sm={9}>
        <Tab.Content>
          <Tab.Pane eventKey="oreo">

            <ListGroup variant="flush">
                <Query query={Oreo}>
                  {
                    ({loading, error, data}) => {
                      if(loading) return <ListGroup.Item>Loading...</ListGroup.Item>
                      if(error) return <div><ListGroup.Item style={{color:"#FF0000"}}> unable to get data from database </ListGroup.Item><ListGroup.Item style={{color:"#FF0000"}}> Makesure at least create dummy data with `node database.js` </ListGroup.Item></div>;
                      return <Fragment>
                        {
                          data.Oreo.map(oreoItem => (
                            <Oreoitem key={oreoItem.id} item={oreoItem} />
                          ))
                        }
                      </Fragment>;
                    }
                  }
                </Query>
            </ListGroup>
          
          </Tab.Pane>
        </Tab.Content>
        <Tab.Content>
          <Tab.Pane eventKey="fiture">

            <ListGroup variant="flush">
                <Query query={FitureTalk}>
                  {
                    ({loading, error, data}) => {
                      if(loading) return <ListGroup.Item>Loading...</ListGroup.Item>
                      if(error) return <ListGroup.Item style={{color:"#FF0000"}}> unable to get data from database </ListGroup.Item>;
                      return <Fragment>
                        {
                          data.FitureTalk.map(oreoItem => (
                            <Oreoitem key={oreoItem.id} item={oreoItem} />
                          ))
                        }
                      </Fragment>;
                    }
                  }
                </Query>
            </ListGroup>
            
          </Tab.Pane>
        </Tab.Content>
      </Col>
    </Row>
  </Tab.Container>
      
    );
  }
}


