import React from 'react';
import { ListGroup } from 'react-bootstrap';


export default function Oreoitem(props) {

	return (
	  	
		<ListGroup.Item action onClick={() => console.log(props.item.name) }>
			{props.item.description}
		</ListGroup.Item>
     
	);
}