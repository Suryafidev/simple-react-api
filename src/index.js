const express = require('express');
const graphqlHTTP = require('express-graphql');
const schema = require('./graphql/graphQL-index');

const app = express();
const cors = require('cors');

app.use(cors());

app.use('/graphql', graphqlHTTP({
    schema: schema,
    graphiql: true,
  }),
);

const PORT = process.env.PORT || 4000;

app.listen(4000, () => console.log(`Server started on port ${PORT}`));