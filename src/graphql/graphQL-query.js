require('dotenv').config();

const { GraphQLObjectType } = require('graphql');


const mysql = require('mysql');


//create connection 
const mysqlConnection = mysql.createConnection({
	host : process.env.DB_HOST,
	user : process.env.DB_USER,
	password : process.env.DB_pass,
	database : process.env.DB_NAME
});

//define mysql connection globally
exports.mysqlConnection = mysqlConnection;

//make mysql query executor globally
getData = ({ query }) => {
  return new Promise((resolve, reject) => {   
    mysqlConnection.query(query, (err, results) => {
      if (err) reject(err + " Query : " + query);
      console.log(query);
      resolve(results);
    });
  });
};

//check connection to mysql
mysqlConnection.query("SELECT version() as __v", (err, rows, fields) => {
	if(err){
		console.log("\x1b[31m" + "\nmysql server active required\n" + "\x1b[97m");
	} else {
      console.log("mysql Version " + rows[0].__v);
   };
});



//////////////////////////////////////////////////////////////////////

//define query 

var Oreo = require( "./Oreo/queries/query.js").Select;

//send query to server

exports.rootQuery = new GraphQLObjectType({
	name : 'queries',
	fields : function () {
   		return {

   			Oreo : Oreo,
   			
		}
	}
});
