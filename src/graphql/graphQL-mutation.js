const {
	GraphQLString,
	GraphQLNonNull,
	GraphQLList,
	GraphQLSchema,
	GraphQLInt,
	GraphQLObjectType
} = require('graphql')


const Oreo_Update  = require( "./Oreo/queries/mutation/Update.js").Update;
const Oreo_Add     = require( "./Oreo/queries/mutation/Add.js").Add;
const Oreo_Delete  = require( "./Oreo/queries/mutation/Delete.js").Delete;


module.exports = {

	Oreo_Update : Oreo_Update,
	Oreo_Add : Oreo_Add,
	Oreo_Delete : Oreo_Delete
};