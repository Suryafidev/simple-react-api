const {
	GraphQLString,
	GraphQLNonNull,
	GraphQLInt,
	GraphQLSchema,
	GraphQLList,
	GraphQLObjectType
} = require('graphql');


const queryType = require('./type.js').queryType;

const mysql = require('mysql');

const mysqlConnection  = require('../../graphQL-query.js').mysqlConnection;

const database_name = "makanoreo";

exports.database_name = database_name;

exports.Select = {
	type : new GraphQLList(queryType),
	args : {
		id : {
			type : GraphQLString
		}
	},
	resolve(parentValue, args) {
		let query = `select * from ` + database_name;    

		if(args.id){
			query += " WHERE id='"+args.id+"'";
		};
		
		return getData({ query: query }).then(value => value);
	}
}
