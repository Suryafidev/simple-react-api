const {
	GraphQLString,
	GraphQLInt,
	GraphQLList,
	GraphQLNonNull,
	GraphQLObjectType,
	GraphQLSchema
} = require("graphql");

const queryType = require("./../type.js").queryType;
const mysqlConnection = require("./../../../graphQL-query.js").mysqlConnection;

var database_name = require("./../query.js").database_name;


exports.Update = {
	type : queryType,
	args : {
		WHEREid: {
			type : GraphQLNonNull(GraphQLString)
		},
		name: {
			type : GraphQLString
		},
		description: {
			type : GraphQLString
		}
	},
	resolve(parentValue, args, error){
		let RowsData = [
			{ name: "name",           value: args.name           },
			{ name: "description",    value: args.description    },
		];

		//combine all agrument into query 
	var Query =
		"UPDATE "
		+ database_name
		+ " set ";
		var argumentChecker = false;
		for(let x = 0; x < RowsData.length; x++){
			if(RowsData[x].value){
				Query += RowsData[x].name + "='" + RowsData[x].value + "',";
				argumentChecker = true;
			}
		}
		if(argumentChecker){
			// this will remove "," at the end of rows or else it will create error
			Query = Query.substring(0, Query.length - 1) + Query.substring(Query.length);
		}


		Query += " WHERE session_id='" + args.WHEREsession_id + "' AND mf_id='" + args.andWHEREmf_id + "' and checkout=0";

		if(args.WHEREsession_id && args.andWHEREmf_id){
			return getData({ query: Query }).then(value => value);
		}
	}
}

