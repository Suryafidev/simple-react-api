require('dotenv').config();

const mysql = require('mysql');

//create connection 
const mysqlConnection = mysql.createConnection({
	host : process.env.DB_HOST,
	user : process.env.DB_USER,
	password : process.env.DB_pass,
});


mysqlConnection.connect(function(err) {
	if (err) throw err;
	console.log("Connected!");

  	mysqlConnection.query("CREATE DATABASE " + process.env.DB_NAME, function (err, result) {
	    if (err) throw "Database " + process.env.DB_NAME + " Already Exist";
	    console.log("Database created");
	});

	const mysqlConnectionCreateTable = mysql.createConnection({
		host : process.env.DB_HOST,
		user : process.env.DB_USER,
		password : process.env.DB_pass,
		database : process.env.DB_NAME
	});

	var sql = "CREATE TABLE makanoreo (id INT AUTO_INCREMENT PRIMARY KEY, name VARCHAR(255), description VARCHAR(255))";

	mysqlConnectionCreateTable.query(sql, function (err, result) {
		if (err) throw "Dummy table already Exist";
		console.log("Dummy Table created");
	});

	sql = "INSERT INTO `makanoreo`(`name`, `description`) VALUES ('langkah pertama','Diputar'), ('langkah kedua','Dijilat'), ('langkah langkah ketiga','Dicelupin')";

	mysqlConnectionCreateTable.query(sql, function (err, result) {
		console.log("Dummy Data created");
		console.log("Press CTRL + C to exit");
	});


	sql = "INSERT INTO `FitureTrain`(`name`, `description`) VALUES ('langkah pertama','Pilih Materinya'), ('langkah kedua','Siapkan Materinya'), (' langkah ketiga','Bahas Matrinya')";

	mysqlConnectionCreateTable.query(sql, function (err, result) {
		console.log("Dummy Data created");
		console.log("Press CTRL + C to exit");
	});
	
});

